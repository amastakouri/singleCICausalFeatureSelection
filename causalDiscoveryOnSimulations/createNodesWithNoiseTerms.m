% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

function [Y] = createNodesWithNoiseTerms(n,nTrials)
mu = 0;
Y = nan(nTrials, n);
for inode = 1 : n
    sigma = random('uniform', 0, 1, 1);
    gm = gmdistribution(mu,sigma);
    Y(:, inode) = random(gm, nTrials);
end
end

