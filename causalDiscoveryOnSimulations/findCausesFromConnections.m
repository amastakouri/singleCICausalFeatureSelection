% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

function [dirCauses, indirCauses] = findCausesFromConnections(connections)
%FINDCAUSESFROMCONNECTIONS Summary of this function goes here
%   Detailed explanation goes here
maxDepth = (size(connections,1)-1)/2;
connections(1 : maxDepth, :) = [];
connections(:, 1 : maxDepth) = [];
connections(end, :) = [];
y1r = connections(1 : end, end);
connections(:, end) = [];
dirCauses = find(y1r);
indirCauses = false(maxDepth, 1);
nonDirNodes = find(~y1r);
children = nan(maxDepth, maxDepth);
for iNode = 1 : length(nonDirNodes)
    node = nonDirNodes(iNode);
    children = findChildren(children, connections, node);
    if sum(children(node, dirCauses)) > 0
        indirCauses(node) = true;
    end
end
indirCauses = find(indirCauses);


