% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

function children = findChildren(children, connections, node)
%FINDCHILDRED Summary of this function goes here
%   Detailed explanation goes here
if sum(isnan(children(node, :))) == 0
    return;
end

children1 = connections(node, :);
children1Nodes = find(children1);
nChildren = length(children1Nodes);
children(node, :) = children1;
if nChildren > 0
    for iChild = 1 : nChildren
        nodeChild = children1Nodes(iChild);
        if sum(isnan(children(nodeChild, :))) > 0
            children = findChildren(children, connections, nodeChild);
        end
    end
end

