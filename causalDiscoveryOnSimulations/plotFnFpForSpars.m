% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

function [h, hm1, hm2] = plotFnFpForSpars(iSpars, nSamplesUniqueValues, nodesUniqueValues,...
    statResultsTable, sparsenessUniqueValues, allColors)

samplesToPlot = statResultsTable.nNodes == nodesUniqueValues &...
    statResultsTable.sparseness == sparsenessUniqueValues(iSpars);
fpAll = statResultsTable.FPAllmean(samplesToPlot);
fnAll = statResultsTable.FNAllmean(samplesToPlot);
hm1 = plot(nSamplesUniqueValues, fpAll,'color', 'black', 'LineWidth', 1.5); hold on;
hm2 = plot(nSamplesUniqueValues, fnAll,'color', 'black', 'LineStyle', '--', 'LineWidth', 1.5); hold on;
plot(nSamplesUniqueValues, fpAll,'color', allColors(iSpars,:), 'Markersize', 9, 'Marker', 's', 'LineWidth', 1.5); hold on;
plot(nSamplesUniqueValues, fnAll,'color', allColors(iSpars,:), 'Markersize', 9, 'Marker', 's', 'LineStyle', '--', 'LineWidth', 1.5); hold on;
h = plot(nSamplesUniqueValues, fpAll,'color', allColors(iSpars,:), 'LineStyle', '-','LineWidth', 1.5); hold on;

end
