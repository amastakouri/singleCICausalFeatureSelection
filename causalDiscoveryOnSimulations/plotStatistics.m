% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

clear all
a = load('statTableCausesGraphsFPandFN_GNChangeSTDPR025_5.mat');
b = load('statTableCausesGraphsFPandFN_GNChangeSTDPR025_20.mat');
c = load('statTableCausesGraphsFPandFN_GNChangeSTDPR025_50.mat');
d = load('statTableCausesGraphsFPandFN_GNChangeSTDPR025_125.mat');
e = load('statTableCausesGraphsFPandFN_GNChangeSTDPR025_200.mat');
statResultsTable = [a.statResultsTable; b.statResultsTable; c.statResultsTable; d.statResultsTable; e.statResultsTable];

allColors = [0, 0.4470, 0.7410; 0.8500, 0.3250, 0.0980; 0.9290, 0.6940, 0.1250; 0.4940, 0.1840, 0.5560];
nodes = statResultsTable.nNodes;
nodesUniqueValues = unique(nodes);
sparseness = statResultsTable.sparseness;
sparsenessUniqueValues = unique(sparseness);
nSamples = statResultsTable.nSamples;
nSamplesUniqueValues = unique(nSamples);
figure
for iNodes = 1:length(nodesUniqueValues)
    subplot(1,length(nodesUniqueValues), iNodes);
    
    h(1) = plotFnFpForSpars(1, nSamplesUniqueValues, nodesUniqueValues(iNodes),statResultsTable,...
        sparsenessUniqueValues, allColors);
    h(2) = plotFnFpForSpars(2, nSamplesUniqueValues, nodesUniqueValues(iNodes),statResultsTable,...
        sparsenessUniqueValues, allColors);
    h(3) = plotFnFpForSpars(3, nSamplesUniqueValues, nodesUniqueValues(iNodes),statResultsTable,...
        sparsenessUniqueValues, allColors);
    [h(4), h(5), h(6)] = plotFnFpForSpars(4, nSamplesUniqueValues, nodesUniqueValues(iNodes),statResultsTable,...
        sparsenessUniqueValues, allColors);
    if iNodes == length(nodesUniqueValues)
        [hleg,icons,plots] =  legend(h, '0.2', '0.3', '0.4', '0.5', 'false positives', 'false negatives',...
        'Interpreter', 'latex','fontSize', 22);
        hleg.Title.Visible = 'on';
        hleg.Title.String = 'Expected value of Bernoulli(p) that defines existence of an edge';
    end
    set(gca,'TickLabelInterpreter','latex', 'FontSize', 16)
    ylabel('False positives \& False negatives', 'interpreter', 'latex', 'fontSize', 26);
    xlabel('\# samples', 'interpreter', 'latex', 'fontSize', 26);
    title(['pairs of nodes: ', num2str(nodesUniqueValues(iNodes))], 'interpreter', 'latex', 'fontSize', 26);
    xticks(nSamplesUniqueValues);
    axis tight
end

%% two figures FP and FN
% fp
clear h
figure
for iNodes = 1:length(nodesUniqueValues)
    subplot(1,length(nodesUniqueValues), iNodes);
    for iSpars = 1:4
        samplesToPlot = statResultsTable.nNodes == nodesUniqueValues(iNodes) &...
            statResultsTable.sparseness == sparsenessUniqueValues(iSpars);
        fpAll = statResultsTable.FPAllmean(samplesToPlot);
        h(iSpars) = plot(nSamplesUniqueValues, fpAll,'color', allColors(iSpars,:),'Marker', 's', 'LineWidth', 1.5); hold on;
    end

    set(gca,'TickLabelInterpreter','latex', 'FontSize', 16)
    ylabel('False positives', 'interpreter', 'latex', 'fontSize', 26);
    xlabel('\# samples', 'interpreter', 'latex', 'fontSize', 26);
    title(['M nodes: ', num2str(nodesUniqueValues(iNodes))], 'interpreter', 'latex', 'fontSize', 26);
    xticks(nSamplesUniqueValues);
    axis tight
end
%fn
clear h
figure
for iNodes = 1:length(nodesUniqueValues)
    subplot(1,length(nodesUniqueValues), iNodes);
    for iSpars = 1:4
        samplesToPlot = statResultsTable.nNodes == nodesUniqueValues(iNodes) &...
            statResultsTable.sparseness == sparsenessUniqueValues(iSpars);
        fnAll = statResultsTable.FNAllmean(samplesToPlot);
        h(iSpars) = plot(nSamplesUniqueValues, fnAll,'color', allColors(iSpars,:),'Marker', 's', 'LineWidth', 1.5); hold on;
    end

    set(gca,'TickLabelInterpreter','latex', 'FontSize', 16)
    ylabel('False negatives', 'interpreter', 'latex', 'fontSize', 26);
    xlabel('\# samples', 'interpreter', 'latex', 'fontSize', 26);
    title(['M nodes: ', num2str(nodesUniqueValues(iNodes))], 'interpreter', 'latex', 'fontSize', 26);
    xticks(nSamplesUniqueValues);
    axis tight
end


