% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

% Main entry point script for the simulations
% simulate graph with assumptions 1-5:


clear all
close all
tic
[statResultsTable]  = calcErrorCausalityFpFn;
toc

