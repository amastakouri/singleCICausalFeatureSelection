% Copyright (C)
% Max Planck Institute for Intelligent Systems,
% Atalanti A. Mastakouri atalanti.mastakouri@tuebingen.mpg.de

function [g, bg] = visualizeGraph(n, allConnections)

 m = 2*n+1;
    nodeNames = strings(1,m);
    for iNodeFullGraph = 1:m
        if iNodeFullGraph <= n
            nodeNames(iNodeFullGraph) = strcat("P_", num2str(iNodeFullGraph));
        elseif iNodeFullGraph > n & iNodeFullGraph <= 2*n
            nodeNames(iNodeFullGraph) = strcat("M_", num2str(iNodeFullGraph - n));
        else
            nodeNames(iNodeFullGraph) = strcat("R");
        end
    end
    bg = biograph(allConnections, nodeNames);
    g = biograph.bggui(bg);
